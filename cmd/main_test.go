package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"parentheses-matching-task-boris/internal"
)

func TestCheckBEqual(t *testing.T) {
	testCases := []struct {
		name  string
		err   error
		input string
	}{
		{
			name:  "ok, valid",
			err:   nil,
			input: "()(){<>}",
		},
		{
			name:  "ok, valid 2",
			err:   nil,
			input: "()(){()}",
		},
		{
			name:  "ok, valid 3",
			err:   nil,
			input: "[({}){}]",
		},
		{
			name:  "error, no closing rune",
			err:   errors.New("no closing rune for {"),
			input: "[({}){}]{",
		},
		{
			name:  "error, not correct close bracket",
			err:   errors.New("opening { is not closed when closing ] was given"),
			input: "[{]}",
		},
		{
			name:  "error, no opening byte for",
			err:   errors.New("no closing rune for ["),
			input: "][{}",
		},
	}

	b := internal.NewByteStack()
	pc := internal.NewPossibleCharacters([][2]byte{
		{'(', ')'},
		{'[', ']'},
		{'{', '}'},
		{'<', '>'},
	})

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := CheckParenthesesCorrect(tc.input, b, pc)
			if tc.err != nil {
				assert.EqualError(t, err, tc.err.Error())
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
