package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/jessevdk/go-flags"

	"parentheses-matching-task-boris/internal"
)

type byteStack interface {
	IsEmpty() bool
	Push(o byte)
	Pop() (byte, bool)
}

type possibleBrackets interface {
	IsOpen(bracket byte) bool
	IsClose(bracket byte) bool
	GetRightCorrespondingBracket(leftBracket byte) (byte, error)
}

// CheckParenthesesCorrect check if parentheses input is correct
func CheckParenthesesCorrect(input string, b byteStack, pc possibleBrackets) error {
	for i := range input {
		if pc.IsOpen(input[i]) {
			b.Push(input[i])
			continue
		} else if pc.IsClose(input[i]) {
			if b.IsEmpty() {
				return errors.New("no opening byte for " + string(input[i]))
			}

			k, _ := b.Pop()
			c, err := pc.GetRightCorrespondingBracket(k)
			if err != nil {
				return err
			}

			if c != input[i] {
				return errors.New(fmt.Sprintf("opening %s is not closed when closing %s was given",
					string(k), string(input[i])))
			}
		} else {
			return errors.New("invalid input:" + input)
		}
	}

	if !b.IsEmpty() {
		i, _ := b.Pop()
		return errors.New("no closing rune for " + string(i))
	}
	return nil
}

type configuration struct {
	Input            string `long:"input" description:"Input string to check" required:"true"`
	PossibleBrackets string `long:"possible_brackets" description:"Pipe separated list of possible brackets" required:"true"`
}

func setupConfiguration() *configuration {
	var cfg configuration

	var flagsParser = flags.NewParser(&cfg, flags.HelpFlag|flags.PassDoubleDash)
	if _, err := flagsParser.Parse(); err != nil {
		if !strings.HasPrefix(err.Error(), "Usage") {
			_, _ = fmt.Fprintln(os.Stderr, fmt.Sprintf("\033[36mERROR: \033[31m%v\033[0m", err))
		}
		flagsParser.WriteHelp(os.Stdout)
		os.Exit(1)
	}
	return &cfg
}

func main() {
	cfg := setupConfiguration()
	pc := internal.NewPossibleCharactersFromPipeString(cfg.PossibleBrackets)
	b := internal.NewByteStack()
	if err := CheckParenthesesCorrect(cfg.Input, b, pc); err != nil {
		fmt.Fprintln(os.Stderr, fmt.Sprintf("\033[36mERROR: \033[31m%v\033[0m", err))
		return
	}
	fmt.Println("OK!")
}
