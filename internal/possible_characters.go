package internal

import (
	"errors"
	"strings"
)

type bracketPair [2]byte

func (b bracketPair) GetLeft() byte {
	return b[0]
}
func (b bracketPair) GetRight() byte {
	return b[1]
}

type PossibleCharacters []bracketPair

// IsOpen check if bracket is actual open bracket
func (p PossibleCharacters) IsOpen(bracket byte) bool {
	for i := range p {
		if p[i].GetLeft() == bracket {
			return true
		}
	}
	return false
}

// IsClose check if bracket is actual close bracket
func (p PossibleCharacters) IsClose(bracket byte) bool {
	for i := range p {
		if p[i].GetRight() == bracket {
			return true
		}
	}
	return false
}

// GetRightCorrespondingBracket return corresponding close bracket
func (p PossibleCharacters) GetRightCorrespondingBracket(leftBracket byte) (byte, error) {
	for i := range p {
		if p[i].GetLeft() == leftBracket {
			return p[i].GetRight(), nil
		}
	}
	return byte(' '), errors.New("no corresponding bracket found")
}

// NewPossibleCharacters constructor for possibleCharacters
func NewPossibleCharacters(items [][2]byte) PossibleCharacters {
	p := make(PossibleCharacters, len(items))
	for i := range items {
		p[i] = bracketPair{items[i][0], items[i][1]}
	}
	return p
}

func NewPossibleCharactersFromPipeString(str string) PossibleCharacters {
	pc := make(PossibleCharacters, 0)
	for _, item := range strings.Split(str, "|") {

		pc = append(pc, bracketPair{item[0], item[1]})
	}
	return pc
}
