package internal

func NewByteStack() *ByteStack {
	b := make(ByteStack, 0)
	return &b
}

type ByteStack []byte

// IsEmpty: check if stack is empty
func (i *ByteStack) IsEmpty() bool {
	return len(*i) == 0
}

// Push a new value onto the stack
func (i *ByteStack) Push(o byte) {
	*i = append(*i, o) // Simply append the new value to the end of the stack
}

// Remove and return top element of stack. Return false if stack is empty.
func (i *ByteStack) Pop() (byte, bool) {
	if i.IsEmpty() {
		return 0, false
	} else {
		index := len(*i) - 1   // Get the index of the top most element.
		element := (*i)[index] // Index into the slice and obtain the element.
		*i = (*i)[:index]      // Remove it from the stack by slicing it off.
		return element, true
	}
}
