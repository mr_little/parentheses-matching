# parentheses-matching-task
Parentheses Matching task by Boris Rostovskiy

## Task: write a function that would check the validity of parentheses `in` the given string. 

### Basic usage ###
* Run: `make build`
* Run: `./cmd/pm --input "()(){()}" --possible_brackets "{}|()|[]"` and notice that sentence `OK` will be printed out.
* Run: `./cmd/pm --input "[({}){}]" --possible_brackets "{}|()|[]"` and notice that sentence `OK` will be printed out.
* Run: `./cmd/pm --input "[{]}" --possible_brackets "{}|()|[]"` and notice that sentence `ERROR: opening { is not closed when closing ] was given` will be printed out.
* Observe more examples in `cmd` *test.
